<?php
require_once "libs/Smarty.class.php";
    
class TaskView {
    private $basehref;
    private $username;
    
    public function __construct(){
        $this->basehref = '//'.$_SERVER['SERVER_NAME'] 
            .dirname($_SERVER['PHP_SELF']).'/'; 
        
        $this->username = $_SESSION['USERNAME'];
    }

    public function showTasks($tareas)
    {
        $smarty = new Smarty();
        //equivalente: smarty("archivo.tpl", $basehref, $tareas, $titulo)
        $smarty->assign('titulo',"Lista de tareas");
        $smarty->assign('listatareas',$tareas);
        $smarty->assign('basehref',$this->basehref);
        $smarty->assign('username',$this->username);
        $smarty->display('templates/taskList.tpl');
    }
    
    function showDetails($tarea, $partial)
    {
        $smarty = new Smarty();
        $smarty->assign('titulo',"Detalle de tarea");
        $smarty->assign('tarea',$tarea);
        $smarty->assign('basehref',$this->basehref);
        $smarty->assign('partial',$partial);
        $smarty->display('templates/taskDetail.tpl');
    }
}