<?php

require_once 'ApiController.php';
require_once './models/TaskModel.php';

class TaskApiController extends ApiController {

    public function __construct() {
        parent::__construct();
        $this->model = new TaskModel();
    }

    public function getTasks($params = []) {
        if (empty($params)) {
         $task = $this->model->getTasks();
         $this->view->response($task, 200);
        }
        else {
            $task_id = $params[':ID'];
            $task = $this->model->getTask($task_id);
             if ($task)
                $this->view->response($task, 200);
            else // si no existe la tarea
                $this->view->response("Task id=$task_id not found", 404);
        }

    }

    public function deleteTask($params = []) {
        $task_id = $params[':ID'];
        $task = $this->model->getTask($task_id);

        if ($task) {
            $this->model->deleteTask($task_id);
            $this->view->response("Tarea id=$task_id eliminada con éxito", 200);
        }
        else 
            $this->view->response("Task id=$task_id not found", 404);
    }

    public function saveTask($params = []) {
        $body = $this->getData();
        $titulo = $body->titulo;
        $descripcion = $body->descripcion;
        
        //inserta la tarea y la busca
        $idTask = $this->model->saveTask($titulo, $descripcion);
        $tarea = $this->model->getTask($idTask);

        if ($tarea)
            $this->view->response($tarea, 200);
        else
            $this->view->response("Error al guardar", 500);
    }

    public function editTask($params = []) {
        $task_id = $params[':ID'];
        $tarea = $this->model->getTask($task_id);

        if ($tarea) {
            $body = $this->getData();
            $finalizada = $body->finalizada;
            if ($finalizada == 1) {
                $this->model->endTask($task_id);
                $tarea = $this->model->getTask($task_id);
                $this->view->response($tarea, 200);
            }
        } else
            $this->view->response("Task id=$task_id not found", 404);
    }

}