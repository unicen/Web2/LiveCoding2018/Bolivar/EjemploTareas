<?php
require_once "views/TaskView.php";
require_once "models/TaskModel.php";
require_once "controllers/SecuredController.php";

class TaskController extends SecuredController
{

    public function __construct() {
        parent::__construct();
        $this->model = new TaskModel();
        $this->view = new TaskView();
    }
    
    public function saveTask()
    {
        if(!empty($_POST["titulo"]))
        {
            $titulo = $_POST["titulo"];
            $descripcion = $_POST["descripcion"];

            //var_dump($_FILES['adjunto']); die();
            $model = new TaskModel();

            if($_FILES['adjunto']['type'] == "image/jpg" || $_FILES['adjunto']['type'] == "image/jpeg" ) 
                $model->saveTask($titulo, $descripcion, $_FILES['adjunto']['tmp_name']);
            else
                $model->saveTask($titulo, $descripcion);

                      
        }   
        header("Location: ".HOME);
    }   

    public function deleteTask($id)
    {
        $this->model->deleteTask($id);          
        header("Location: ".HOME);
    }   
    public function endTask($id)
    {
        $this->model->endTask($id);          
        header("Location: ".HOME);
    }   
    public function showDetails($id)
    {
        if(isset($_GET["partial"]))
            $partial = true;
        $tarea = $this->model->getTask($id);
        $this->view->showDetails($tarea, $partial);
    }

    public function showTasks()
    {
        $tareas = $this->model->getTasks();
        $this->view->showTasks($tareas);
    }
    
}
    