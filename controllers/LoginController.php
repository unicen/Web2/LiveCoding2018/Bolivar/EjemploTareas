<?php
include_once 'views/LoginView.php';
include_once 'models/UserModel.php';
include_once 'controllers/Controller.php';

class LoginController extends Controller {


    public function __construct() {
        parent::__construct();
        $this->view = new LoginView();
        $this->model = new UserModel(); 
    }

    public function mostrarLogin() {
        $this->view->mostrarFormLogin();
    }

    public function verify() {
        $username = $_POST['username'];
        $password = $_POST['password'];

        if(!empty($username) && !empty($password)){
            $user = $this->model->getUser($username);
            if((!empty($user)) && password_verify($password, $user['password'])) {
                session_start();
                $_SESSION['USERNAME'] = $username;
                $_SESSION['ID'] = $user["id_usuario"];
                $_SESSION['LAST_ACTIVITY'] = time();
                header("Location: ".HOME);
                die();
            }
            else {
                $this->view->mostrarFormLogin("Usuario o contraseña incorrecta");
            }
        }  

    }

    public function logout() {
        session_start();
        session_destroy();
        header('Location: '.LOGIN);
        die(); // buena practica hacer logout
    }
}