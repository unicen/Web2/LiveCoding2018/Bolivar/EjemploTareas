{include file="header.tpl"}
    <div class="row">

     <div class="col-12">
        <h1>{$titulo}</h1>
     </div>

    <div class="col-5">
        <form method="POST" action="save" enctype="multipart/form-data">
            
            <div class="form-group">
                <label for="titulo">Tarea</label>
                <input type="text" name="titulo" id="titulo" class="form-control">
            </div>

            <div class="form-group">
                <label for="descripcion">Descripción</label>
                <textarea class="form-control" name="descripcion" id="descripcion" rows="3"></textarea>
            </div>

            <!-- nuevo input de archivo -->
            <div class="form-group">
                <label for="adjunto">Adjuntar imagen</label>
                <input type="file" name="adjunto" class="form-control-file" id="adjunto" accept="image/jpeg">
            </div>

            <button type="submit" class="btn btn-primary">AGREGAR</button>
        </form> 
    </div> 

    <div class="col-7">
        <div id="container-tareas">
            
        </div>
    </div>
</div>  

{include file="footer.tpl"}
