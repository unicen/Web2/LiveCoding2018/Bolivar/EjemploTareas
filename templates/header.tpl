<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{$basehref}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/style.css">
    <title>{$titulo}</title>
</head>
<body>
   <nav class="nav">
        <a class="nav-link" href="">Tareas</a>
        {if isset($username)} {* si no está vacio *}
            <a class="nav-link" href="logout">Cerrar Sesion ({$username})</a>
        {/if}
    </nav>
    <article id="mainContainer" class="container">
    
