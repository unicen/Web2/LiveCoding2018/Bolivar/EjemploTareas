<?php

require_once 'Router.php';
require_once 'api/TaskApiController.php';

$r = new Router();

// rutas de la api
$r->addRoute("tareas","GET", "TaskApiController", "getTasks");
$r->addRoute("tareas/:ID", "GET", "TaskApiController", "getTasks");
$r->addRoute("tareas/:ID", "DELETE", "TaskApiController", "deleteTask");
$r->addRoute("tareas", "POST", "TaskApiController", "saveTask");
$r->addRoute("tareas/:ID","PUT", "TaskApiController", "editTask");

//run
$r->route($_GET['resource'], $_SERVER['REQUEST_METHOD']);
