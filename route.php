<?php

require_once 'controllers/TaskController.php';
require_once 'controllers/LoginController.php';

// constantes
define('ACTION', 0); 
define('ID', 1);

// si no indica "action" forzamos asi entra al default
if (!isset($_GET['action']))
    $_GET['action'] = '';

// parsea (separa) la url (si viene "sumar/5/8" => [sumar, 5, 8])
$action = $_GET['action'];
$partesURL = explode("/", $action);

// decide que acción tomar en base a la url
switch ($partesURL[ACTION]) {
    case 'login': 
        $controller = new LoginController();
        $controller->mostrarLogin();
    break;
    case 'verify': 
        $controller = new LoginController();
        $controller->verify();
    break;
    case 'logout':
        $controller = new LoginController();
        $controller->logout();
        break;
    case 'save': 
        $controller = new TaskController();
        $controller->saveTask();
        break;
    case 'end': 
        $controller = new TaskController();
        $controller->endTask($partesURL[ID]);
        break;
    case 'edit': 

        break;
    case 'delete': 
        $controller = new TaskController();
        $controller->deleteTask($partesURL[ID]);
        break;
    case 'tareas':
        $controller = new TaskController();
        $controller->showDetails( $partesURL[ID]);
        break;
    case 'ver': 
    default: 
        $controller = new TaskController();
        $controller->showTasks();
        break;
}