<?php 
class TaskModel
{
    private $db_connection;
    
    function __construct()
    {
        $this->db_connection = new PDO('mysql:host=localhost;'
        .'dbname=db_tareas;charset=utf8'
        , 'root', '');
    }
    function getTasks()
    {
        $sentencia = $this->db_connection->prepare( "select * from tarea");
        $sentencia->execute();
        $tasks = $sentencia->fetchAll(PDO::FETCH_OBJ);

        foreach ($tasks as $task) 
            $task->finalizada = $task->finalizada ? true : false;

        return $tasks;
    }

    function getTask($id)
    {
        $sentencia = $this->db_connection->prepare( "select * from tarea"
                ." WHERE id_tarea = ?");
        $sentencia->execute(array($id));
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }

    function saveTask($title, $desc = "", $img = null)
    {
        $pathImg = null;
        if ($img)
            $pathImg = $this->uploadImage($img);
        //preparar la sentecia SQL con PDO seguro (los ?)
        $sentencia = $this->db_connection->prepare("INSERT INTO tarea(titulo,descripcion,imagen) VALUES(?,?,?)");
        //pasar los parametros y ejecutar en la BBDD
        $sentencia->execute(array($title, $desc, $pathImg));

        return $this->db_connection->lastInsertId();
    }

    private function uploadImage($image){
        $target = 'img/task/' . uniqid() . '.jpg';
        move_uploaded_file($image, $target);
        return $target;
    }


    function deleteTask($id_tarea)
    {
        $sentencia = $this->db_connection->prepare( 
        "delete from tarea where id_tarea=?");
        $sentencia->execute(array($id_tarea));
    }


    function endTask($id_tarea)
    {
        $sentencia = $this->db_connection->prepare( 
        "UPDATE tarea SET finalizada = 1 WHERE id_tarea=?");
        $sentencia->execute(array($id_tarea));
    }

}